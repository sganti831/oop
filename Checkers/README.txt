README

This project simulates a Checkers game.
The source code within the 'Core' directory handles the game logic, and the code within the 'UI' directory displays the game UI.

Sidenotes:
1. The UI has an option for the user to display the game rules
