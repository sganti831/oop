package com.srutiganti.ProjectDeliverable2.UI;
import com.srutiganti.ProjectDeliverable2.Core.CheckersLogic;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This is a program that simulates the game checkers and implements
 * a console User Interface.
 *
 * @author Sruti Ganti
 * @version 2/19/21, Version 1.0
 */

public class CheckersTextConsole {
    //declare private instance variables
    private CheckersLogic logic;

    /**
     * Default class constructor
     */
    CheckersTextConsole() { }

    /**
     * This method displays the menu of options for the user
     * @since 1.0
     */
    public void displayMenu() {
        try {
            //instantiate Scanner class
            Scanner scan = new Scanner(System.in);

            //declare variables to store user input
            int userInput;
            String userChoice = "";

            do {
                System.out.println("Checkers Game Menu \n" +
                        "1. Start a new game  \n" +
                        "2. Checkers Rules \n" +
                        "3. Quit");
                userInput = scan.nextInt();
                if(! (userInput == 1 || userInput == 2 || userInput == 3)) {
                    throw new InputMismatchException();
                }
                System.out.println();

                //introduce switch case
                switch (userInput) {
                    case 1:
                        //instance of CheckersLogic
                        logic = new CheckersLogic();

                        //start new game
                        logic.startNewGame();
                        while (logic.checkGameStatus() == true) {
                            System.out.println();
                            userChoice = scan.next();
                            System.out.println();
                            logic.turn(userChoice);
                        }
                        break;
                    case 2:
                        System.out.println("   CHECKERS RULES \n\n" +
                                "   1. Checkers is a two player game. \n" +
                                "   2. Each player (playerX and playerO) begins with 12 pieces. \n" +
                                "   3. Each new game begins with playerX. \n" +
                                "   4. Players must alternate turns. \n" +
                                "   5. A player may not move an opponent's piece. \n" +
                                "   6. Moves may only be made in the forward and diagonal direction. \n" +
                                "   7. Capturing an opponent's piece entails moving two consecutive \n" +
                                "      steps in the same line, jumping over the piece on the first step. \n" +
                                "   8. Multiple enemy pieces may be captured in a single turn provided \n" +
                                "      this is done by successive jumps made by a single piece. \n" +
                                "   9. Regarding rule 8, the jumps do not need to be in the same \n" +
                                "      line and may \"zigzag\" (change diagonal direction). \n" +
                                "  10. Pieces may only move or jump in the forward/diagonal direction. \n" +
                                "  11. A player may not move an empty piece. \n" +
                                "  12. The first player to lose all of their pieces, loses the game. \n" +
                                "  13. If a player is put in a position where they cannot move, in \n" +
                                "      other words, the player is boxed in, then they lose the game. \n" +
                                "  14. A player may not jump or capture their own piece. \n\n" +
                                "   Aim of the Game: Capture all the opponent's pieces or render them \n" +
                                "   unable to move. ");
                        System.out.println();
                        break;
                    case 3:
                        System.out.println("Thank you for playing!");
                        break;
                }
                //exit do-while loop when user does not select 1
            } while(userInput != 3);
        } catch (InputMismatchException e) {
            System.out.println("Invalid option");
        }
    }


    /**
     * This is the main method that is executed which interacts
     * with the user
     * @param args
     */
    public static void main(String[] args) {
        //instantiate CheckersTextConsole;
        CheckersTextConsole checkers = new CheckersTextConsole();

        System.out.println("Welcome to Checkers! \n");
        System.out.println("Please select an option from the following menu: \n");
        checkers.displayMenu();
    }
}
