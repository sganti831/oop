package com.srutiganti.ProjectDeliverable2.Core;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Locale;

/**
 * This class implements the Checkers game logic.
 *
 * @author Sruti Ganti
 * @version 2/20/21, Version 1.0
 */

public class CheckersLogic {
    //declare private instance variables
    private CheckersBoard board;
    private boolean gameinProg;
    private int currentPlayer;
    private int newGame;

    /**
     * Class constructor
     * Initializes instance variables
     */
    public CheckersLogic() {
        gameinProg = false;
        currentPlayer = 0;
        newGame = 0;
    }

    /**
     * This method starts a new game and displays the
     * checkers board
     * @since 1.0
     */
    public void startNewGame() {
        //first, check if there is a game in progress
        if(gameinProg == true && newGame == 0) {
            System.out.println("Game currently in progress. Please finish this game" +
                    "before starting a new one. ");
        }
        else {
            if(newGame == 0) {
                //set up board
                board = new CheckersBoard();
                System.out.println(board.setUpBoard());

                //set the current player to playerX
                currentPlayer = board.getX();

                //PlayerX's turn
                System.out.println("Begin game. PlayerX-your turn");
                System.out.print("Choose the cell position of the piece to be moved," +
                        " and the new position to move the piece to. e.g., 3a-4b");
                newGame++;

            }
            else {
                //not a new game, switching turns
                if(currentPlayer == board.getX()) {
                    System.out.println("PlayerX-your turn. ");
                    System.out.print("Choose the cell position of the piece to be moved," +
                            " and the new position to move the piece to.");
                }
                else {
                    System.out.println("PlayerO-your turn. ");
                    System.out.print("Choose the cell position of the piece to be moved," +
                            " and the new position to move the piece to.");
                }
            }

            //set gameStatus to true
            gameinProg = true;
        }
    }

    /**
     * This method switches turns between PlayerX
     * and Player once a player has made a valid move
     * @since 1.0
     * @param userChoice
     */
    public void turn(String userChoice) {
        //handles invalid input for move
        if(!(Character.isDigit(userChoice.charAt(0))) || !(Character.isLetter(userChoice.charAt(1)))
        || !(userChoice.charAt(2) == '-') || !(Character.isDigit(userChoice.charAt(3)))
        || !(Character.isLetter(userChoice.charAt(4))) || (userChoice.charAt(1) > 'h' || userChoice.charAt(1) < 'a')
            || (userChoice.charAt(4) < 'a' || userChoice.charAt(4) > 'h') || !(userChoice.length() == 5)
        || (userChoice.charAt(0) < '1' || userChoice.charAt(0) > '8') || (userChoice.charAt(3) < '1'
        || userChoice.charAt(3) > '8')) {
            System.out.println("Invalid input. \n" +
                    "Please try again. \n");
        }
        else {
            //parses through userString
            char oldRowChar = userChoice.charAt(0);
            int oldRow = oldRowChar - '0';
            char oldCol = userChoice.charAt(1);
            char newRowChar = userChoice.charAt(3);
            int newRow = newRowChar - '0';
            char newCol = userChoice.charAt(4);

            if(gameinProg == true) {
                //checks if current player is PlayerX
                if(this.currentPlayer == board.getX()) {
                    if(board.choosePieceToMoveX(oldRow, oldCol, newRow, newCol) == true) {
                        //switch player to PlayerO
                        currentPlayer = board.getO();
                        startNewGame();
                    }
                }
                else {
                    if(board.choosePieceToMoveO(oldRow, oldCol, newRow, newCol) == true) {
                        //set current player to playerX
                        currentPlayer = board.getX();
                        startNewGame();
                    }
                }
            }
        }
    }

    /**
     * This method ends the current checkers game
     * @since 1.0
     */
    public void endGame() {
        //first check if there is a game in progress
        if(gameinProg == false) {
            System.out.println("Please start a game first. \n");
        }
        else {
            System.out.println("Your game has now ended.");
        }
        //set gameStatus to false
        gameinProg = false;

        //set new game back to zero
        newGame --;
    }

    /**
     * This method checks the game status and determines
     * whether the current checkers game should continue
     * @since 1.0
     * @return contGame
     */
    public boolean checkGameStatus() {
        boolean contGame = false;
        //if either player captures 12 pieces or loses all pieces
        if((board.getXcapturedPieces() == 12) || (board.getOcapturedPieces() == 12)
        || (board.getXtotalPieces() == 0) || (board.getOtotalPieces() == 0)) {
            //check who won
            if(board.getXcapturedPieces() == 12) {
                //this means playerX won.
                System.out.println("PlayerX captured all of PlayerO's pieces. \n" +
                        "PlayerX won, PlayerO lost. \n");
                //set the game status to false
                contGame = false;
                //end current game
                endGame();
                System.out.println();
            }
            else {
                //this means playerO won
                System.out.println("PlayerO captured all of PlayerX's pieces. \n" +
                        "PlayerO won, PlayerX lost. \n");
                //set game status to false
                contGame = false;
                //end current game
                endGame();
                System.out.println();
            }
        }
        //otherwise, continue current game
        else{
            contGame = true;
        }
        return contGame;
    }
}
