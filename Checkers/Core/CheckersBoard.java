package com.srutiganti.ProjectDeliverable2.Core;

import java.util.ArrayList;

/**
 * This class acts as the checker board for the checkers game. Moreover, this class
 * is responsible for checking valid moves and updating the checkers board.
 *
 * @author Sruti Ganti
 * @version 2/19/21, Version 1.0
 */

public class CheckersBoard {
    //declare private instance variables
    private static String[][] checkersBoard = new String[9][9];
    private static final int X = 1;
    private static final int O = 2;
    private static int XtotalPieces = 12;
    private static int OtotalPieces = 12;
    private static int XcapturedPieces = 0;
    private static int OcapturedPieces = 0;

    /**
     * Default class constructor
     */
    public CheckersBoard() {

    }

    /**
     * This method returns PlayerX's value
     * @since 1.0
     * @return X
     */
    public int getX() {
        return this.X;
    }

    /**
     * This method returns PlayerO's value
     * @since 1.0
     * @return O
     */
    public int getO() {
        return this.O;
    }

    /**
     * This method returns the number of total pieces
     * PlayerX still has on the checkers board
     * @since 1.0
     * @return XtotalPieces
     */
    public int getXtotalPieces() {
        return this.XtotalPieces;
    }

    /**
     * This method returns the number of total pieces
     * PlayerO still has on the checkers board
     * @since 1.0
     * @return OtotalPieces
     */
    public int getOtotalPieces() {
        return this.OtotalPieces;
    }

    /**
     * This method returns the total number of
     * pieces captured by PlayerX
     * @since 1.0
     * @return XcapturedPieces
     */
    public int getXcapturedPieces() {
        return this.XcapturedPieces;
    }

    /**
     * This method returns the total number of
     * pieces captured by PlayerO
     * @since 1.0
     * @return OcapturedPieces
     */
    public int getOcapturedPieces() {
        return this.OcapturedPieces;
    }

    /**
     * This method sets up the checkers board and returns it
     * as a string
     * @since 1.0
     * @return toString(checkersBoard)
     */
    public String setUpBoard() {
        //set rows 1-3
        for(int row = 1; row < 4; row+=2) {
            for(int col = 0; col < 7; col+=2) {
                checkersBoard[row][col] = "| _";
            }
            for(int col = 1; col < 7; col+=2) {
                checkersBoard[row][col] = "| o";
                checkersBoard[1][7] = "| o |";
                checkersBoard[3][7] = "| o |";
            }
        }
        for(int row = 2; row < 3; row++) {
            for(int col = 0; col < 7; col+=2) {
                checkersBoard[row][col] = "| o";
            }
            for(int col = 1; col < 7; col+=2) {
                checkersBoard[row][col] = "| _";
                checkersBoard[2][7] = "| _ |";
            }
        }

        //set rows 4 and 5
        for(int row = 4; row < 6; row++) {
            for(int col = 0; col < 8; col++) {
                checkersBoard[row][col] = "| _";
                checkersBoard[4][7] = "| _ |";
                checkersBoard[5][7] = "| _ |";
            }
        }

        //set rows 6-8
        for(int row = 6; row < 9; row+=2) {
            for(int col = 0; col < 8; col+=2) {
                checkersBoard[8][1] = "| x";
                checkersBoard[row][col] = "| x";
            }
            for(int col = 1; col < 8; col +=2) {
                checkersBoard[row][col] = "| _";
                checkersBoard[6][7] = "| _ |";
                checkersBoard[8][7] = "| _ |";
            }
        }
        for(int row = 7; row < 8; row++) {
            for(int col = 0; col < 8; col+=2) {
                checkersBoard[row][col] = "| _";
            }
            for(int col = 1; col < 8; col +=2) {
                checkersBoard[row][col] = "| x";
                checkersBoard[7][7] = "| x |";
            }
        }
        //filling in letters for row 0
        checkersBoard[0][0] = "";
        checkersBoard[0][1] = " a  ";
        checkersBoard[0][2] = "b  ";
        checkersBoard[0][3] = "c  ";
        checkersBoard[0][4] = "d  ";
        checkersBoard[0][5] ="e  ";
        checkersBoard[0][6] = "f  ";
        checkersBoard[0][7] = "g  ";
        checkersBoard[0][8] = "h  ";

        //filling in numbers for column 9
        checkersBoard[1][8] = "8";
        checkersBoard[2][8] = "7";
        checkersBoard[3][8] = "6";
        checkersBoard[4][8] = "5";
        checkersBoard[5][8] = "4";
        checkersBoard[6][8] = "3";
        checkersBoard[7][8] = "2";
        checkersBoard[8][8] = "1";

        return toString(checkersBoard);
    }

    /**
     * This method displays the checkers board and
     * returns it as a string
     * @since 1.0
     * @return toString(checkersBoard)
     */
    public String displayCheckersBoard() {
        return toString(checkersBoard);
    }

    /**
     * This method takes in the row and column of the initial position,
     * as well as the row and column of the new position and makes
     * makes PlayerX's move
     * @since 1.0
     * @param initRow
     * @param initCol
     * @param nextRow
     * @param nextCol
     * @return verifiedX
     */
    public boolean choosePieceToMoveX(int initRow, char initCol, int nextRow, char nextCol) {
        boolean verifiedX = false;
        int oldRow = 0;
        int oldCol = 0;
        int newRow = 0;
        int newCol = 0;

        //convert entered int to proper board int

        //convert initCol char to int
        if(initCol == 'a') {
            oldCol = 0;
        }
        if(initCol == 'b') {
            oldCol = 1;
        }
        if(initCol == 'c') {
            oldCol = 2;
        }
        if(initCol == 'd') {
            oldCol = 3;
        }
        if(initCol == 'e') {
            oldCol = 4;
        }
        if(initCol == 'f') {
            oldCol = 5;
        }
        if(initCol == 'g') {
            oldCol = 6;
        }
        if(initCol == 'h') {
            oldCol = 7;
        }

        //convert nextCol char to int
        if(nextCol == 'a') {
            newCol = 0;
        }
        if(nextCol == 'b') {
            newCol = 1;
        }
        if(nextCol == 'c') {
            newCol = 2;
        }
        if(nextCol == 'd') {
            newCol = 3;
        }
        if(nextCol == 'e') {
            newCol = 4;
        }
        if(nextCol == 'f') {
            newCol = 5;
        }
        if(nextCol == 'g') {
            newCol = 6;
        }
        if(nextCol == 'h') {
            newCol = 7;
        }

        //convert initRow to int
        if(initRow == 8) {
            oldRow = 1;
        }
        if(initRow == 7) {
            oldRow = 2;
        }
        if(initRow == 6) {
            oldRow = 3;
        }
        if(initRow == 5) {
            oldRow = 4;
        }
        if(initRow == 4) {
            oldRow = 5;
        }
        if(initRow == 3) {
            oldRow = 6;
        }
        if(initRow == 2) {
            oldRow = 7;
        }
        if(initRow == 1) {
            oldRow = 8;
        }

        //convert nextRow to int
        if(nextRow == 8) {
            newRow = 1;
        }
        if(nextRow == 7) {
            newRow = 2;
        }
        if(nextRow == 6) {
            newRow = 3;
        }
        if(nextRow == 5) {
            newRow = 4;
        }
        if(nextRow == 4) {
            newRow = 5;
        }
        if(nextRow == 3) {
            newRow = 6;
        }
        if(nextRow == 2) {
            newRow = 7;
        }
        if(nextRow == 1) {
            newRow = 8;
        }

        //validate move
        if(validateMoveX(oldRow, oldCol, newRow, newCol) == true) {
            //make PlayerX's move
            checkersBoard[newRow][newCol] = checkersBoard[oldRow][oldCol];
            verifiedX = true;
            if(oldCol == 7) {
                checkersBoard[oldRow][oldCol] = "| _ |";
            }
            else {
                checkersBoard[oldRow][oldCol] = "| _";
            }
            if(newCol == 7) {
                checkersBoard[newRow][newCol] = "| x |";
            }
            else {
                checkersBoard[newRow][newCol] = "| x";
            }

            //check if the move is a jump
            if(Math.abs(oldRow - newRow) == 2) {
                int pieceJumpRow = (oldRow + newRow) / 2;
                int pieceJumpCol = (oldCol + newCol) / 2;
                if(pieceJumpCol == 7) {
                    checkersBoard[pieceJumpRow][pieceJumpCol] = "| _ |";
                }
                else {
                    checkersBoard[pieceJumpRow][pieceJumpCol] = "| _";
                }
                verifiedX = true;
                //increment playerO taken pieces
                XcapturedPieces++;

                //decrement playerX total pieces
                OtotalPieces--;
            }
        }
        else {
            verifiedX = false;
        }
         System.out.println(displayCheckersBoard());
        return verifiedX;
    }

    /**
     * This method takes in the row and column of the initial position,
     * as well as the row and column of the new position and makes
     * makes PlayerO's move
     * @since 1.0
     * @param initRow
     * @param initCol
     * @param nextRow
     * @param nextCol
     * @return verifiedO
     */

    public boolean choosePieceToMoveO(int initRow, char initCol, int nextRow, char nextCol) {
        boolean verifiedO = false;
        int oldRow = 0;
        int oldCol = 0;
        int newRow = 0;
        int newCol = 0;

        //convert entered int to proper board int

        //convert initCol char to int
        if(initCol == 'a') {
            oldCol = 0;
        }
        if(initCol == 'b') {
            oldCol = 1;
        }
        if(initCol == 'c') {
            oldCol = 2;
        }
        if(initCol == 'd') {
            oldCol = 3;
        }
        if(initCol == 'e') {
            oldCol = 4;
        }
        if(initCol == 'f') {
            oldCol = 5;
        }
        if(initCol == 'g') {
            oldCol = 6;
        }
        if(initCol == 'h') {
            oldCol = 7;
        }

        //convert nextCol char to int
        if(nextCol == 'a') {
            newCol = 0;
        }
        if(nextCol == 'b') {
            newCol = 1;
        }
        if(nextCol == 'c') {
            newCol = 2;
        }
        if(nextCol == 'd') {
            newCol = 3;
        }
        if(nextCol == 'e') {
            newCol = 4;
        }
        if(nextCol == 'f') {
            newCol = 5;
        }
        if(nextCol == 'g') {
            newCol = 6;
        }
        if(nextCol == 'h') {
            newCol = 7;
        }

        //convert initRow to int
        if(initRow == 8) {
            oldRow = 1;
        }
        if(initRow == 7) {
            oldRow = 2;
        }
        if(initRow == 6) {
            oldRow = 3;
        }
        if(initRow == 5) {
            oldRow = 4;
        }
        if(initRow == 4) {
            oldRow = 5;
        }
        if(initRow == 3) {
            oldRow = 6;
        }
        if(initRow == 2) {
            oldRow = 7;
        }
        if(initRow == 1) {
            oldRow = 8;
        }

        //convert nextRow to int
        if(nextRow == 8) {
            newRow = 1;
        }
        if(nextRow == 7) {
            newRow = 2;
        }
        if(nextRow == 6) {
            newRow = 3;
        }
        if(nextRow == 5) {
            newRow = 4;
        }
        if(nextRow == 4) {
            newRow = 5;
        }
        if(nextRow == 3) {
            newRow = 6;
        }
        if(nextRow == 2) {
            newRow = 7;
        }
        if(nextRow == 1) {
            newRow = 8;
        }

        //validate move
        if(validateMoveO(oldRow, oldCol, newRow, newCol) == true) {
            //make PlayerO's move
            checkersBoard[newRow][newCol] = checkersBoard[oldRow][oldCol];
            verifiedO = true;
            if(oldCol == 7) {
                checkersBoard[oldRow][oldCol] = "| _ |";
            }
            else {
                checkersBoard[oldRow][oldCol] = "| _";
            }

            if(newCol == 7) {
                checkersBoard[newRow][newCol] = "| o |";
            }
            else {
                checkersBoard[newRow][newCol] = "| o";
            }

            //check if the move is a jump
            if(Math.abs(oldRow - newRow) == 2) {
                int pieceJumpRow = (oldRow + newRow) / 2;
                int pieceJumpCol = (oldCol + newCol) / 2;
                if(pieceJumpCol == 7) {
                    checkersBoard[pieceJumpRow][pieceJumpCol] = "| _ |";
                }
                else {
                    checkersBoard[pieceJumpRow][pieceJumpCol] = "| _";
                }
                verifiedO = true;
                //increment playerO taken pieces
                OcapturedPieces++;

                //decrement playerX total pieces
                XtotalPieces--;
            }
        }
        else {
            verifiedO = false;
        }

        System.out.println(displayCheckersBoard());
        return verifiedO;
    }

    /**
     * This method checks to see if PlayerX's move is valid
     * @since 1.0
     * @param oldRow
     * @param oldCol
     * @param newRow
     * @param newCol
     * @return validX
     */
    public boolean validateMoveX(int oldRow, int oldCol, int newRow, int newCol) {
        boolean validX = false;
        //ensure opponent's piece is not moved
        if((checkersBoard[oldRow][oldCol].equals("| o")) ||
                (checkersBoard[oldRow][oldCol].equals("| o |"))) {
            System.out.println("Not a valid move, cannot move opponent's pieces.\n" +
                    "Please try again.\n");
            validX = false;
        }
        //ensure only moving forward diagonally
        else if(oldCol == newCol) {
            System.out.println("Not a valid move, can only move forward diagonally.\n" +
                    "Please try again.\n");
            System.out.println();
            validX = false;
        }
        //cannot move side to side
        else if(oldRow == newRow) {
            validX = false;
            System.out.println("Not a valid move, can only move forward diagonally.\n" +
                    "Please try again.\n");
            System.out.println();
        }
        //cannot move backwards
        else if(newRow > oldRow) {
            System.out.println("Not a valid move, cannot move backwards.\n" +
                    "Please try again.\n");
            validX = false;
        }
        //invalid row
        else if((oldRow < 1 || oldRow > 8) || (newRow < 1 || newRow > 8)) {
            System.out.println("Not a valid move, the row you would like to" +
                    " move to is out of bounds.\n" +
                    "Please try again.");
            System.out.println();
            validX = false;
        }
        //invalid column
        else if((oldCol < 0 || oldCol > 7) || (newCol < 0 || newCol > 7)) {
            System.out.println("Not a valid move, the column you would like" +
                    " to move to is out of bounds.\n" +
                    "Please try again.");
            System.out.println();
            validX = false;
        }
        //moving empty piece
        else if(checkersBoard[oldRow][oldCol].equals("| _") || checkersBoard[oldRow][oldCol].equals("| _ |")) {
            System.out.println("Not a valid move, you are trying to move a piece" +
                    " that does not exist.\n" +
                    "Please try again.");
            System.out.println();
            validX = false;
        }
        //trying to jump invalid piece
        else if(checkersBoard[newRow][newCol].equals("| x") || checkersBoard[newRow][newCol].equals("| x |")) {
            System.out.println("Not a valid move, you are trying to take one of your own pieces.\n" +
                    "Please try again.");
            System.out.println();
            validX = false;
        }
        //otherwise, all other moves are valid
        else {
            validX = true;
        }
        return validX;
    }

    /**
     * This method checks to see if PlayerO's move is valid
     * @since 1.0
     * @param oldRow
     * @param oldCol
     * @param newRow
     * @param newCol
     * @return validO
     */
    public boolean validateMoveO(int oldRow, int oldCol, int newRow, int newCol) {
        boolean validO = false;
        //cannot move opponent's piece
        if((checkersBoard[oldRow][oldCol].equals("| x")) ||
                (checkersBoard[oldRow][oldCol].equals("| x |"))) {
            System.out.println("Not a valid move, cannot move opponent's pieces.\n" +
                    "Please try again.\n");
            validO = false;
        }
        //can only move forward diagonally
        else if(oldCol == newCol) {
            System.out.println("Not a valid move, can only move forward diagonally.\n" +
                    "Please try again. ");
            System.out.println();
            validO = false;
        }
        //cannot move side to sde
        else if(oldRow == newRow) {
            System.out.println("Not a valid move, can only move forward diagonally.\n" +
                    "Please try again. ");
            System.out.println();
            validO = false;
        }
        //cannot move backwards
        else if(newRow < oldRow) {
            System.out.println("Not a valid move, cannot move backwards.\n" +
                    "Please try again. ");
            System.out.println();
            validO = false;
        }
        //invalid row
        else if((oldRow < 1 || oldRow > 8) || (newRow < 1 || newRow > 8)) {
            System.out.println("Not a valid move, the row you would like to" +
                    " move to is out of bounds.\n" +
                    "Please try again. ");
            System.out.println();
            validO = false;
        }
        //invalid column
        else if((oldCol < 0 || oldCol > 7) || (newCol < 0 || newCol > 7)) {
            System.out.println("Not a valid move, the columns you would like" +
                    " to move to is out of bounds.\n" +
                    "Please try again. ");
            System.out.println();
            validO = false;
        }
        //trying to move empty piece
        else if(checkersBoard[oldRow][oldCol].equals("| _") || checkersBoard[oldRow][oldCol].equals("| _ |")) {
            System.out.println("Not a valid move, you are trying to move a piece" +
                    " that does not exist.\n" +
                    "Please try again. ");
            System.out.println();
            validO = false;
        }
        //trying to jump invalid piece
        else if(checkersBoard[newRow][newCol].equals("| o") || checkersBoard[newRow][newCol].equals("| o |")) {
            System.out.println("Not a valid move, you are trying to take one of your own pieces.\n" +
                    "Please try again. ");
            System.out.println();
            validO = false;
        }
        //otherwise, all other moves are valid
        else {
            validO = true;
        }
        return validO;
    }

    /**
     * This method checks if PlayerX has any legal moves left
     * @since 1.0
     * @return legalXMovesLeft
     */
    public ArrayList<Boolean> legalMovesX() {
       ArrayList<Boolean> legalXMovesLeft = new ArrayList<Boolean>();
       for(int row = 1; row < 9; row++) {
           for(int col = 0; col < 8; col++) {
                if(checkersBoard[row][col].equals("| x") || checkersBoard[row][col].equals("| x |")) {
                    if(validateMoveX(row, col, row+1, col+1) == true) {
                       legalXMovesLeft.add(true);
                    }
                    else {
                        legalXMovesLeft.add(false);
                    }
                    if(validateMoveX(row, col, row-1, col+1) == true) {
                        legalXMovesLeft.add(true);
                    }
                    else {
                        legalXMovesLeft.add(false);
                    }
                    if(validateMoveX(row, col, row+1, col-1) == true) {
                        legalXMovesLeft.add(true);
                    }
                    else {
                        legalXMovesLeft.add(false);
                    }
                    if(validateMoveX(row,col, row-1, col-1) == true) {
                        legalXMovesLeft.add(true);
                    }
                    else {
                        legalXMovesLeft.add(false);
                    }
                }
           }
       }
       return legalXMovesLeft;
    }

    /**
     * This method checks if PlayerO has any legal moves left
     * @since 1.0
     * @return legalOMovesLeft
     */
    public ArrayList<Boolean> legalMovesO() {
        ArrayList<Boolean> legalOMovesLeft = new ArrayList<>();
        for(int row = 1; row < 9; row++) {
            for(int col = 0; col < 8; col++) {
                if(checkersBoard[row][col].equals("| o") || checkersBoard[row][col].equals("| o |")) {
                    if(validateMoveO(row, col, row+1, col+1) == true) {
                        legalOMovesLeft.add(true);
                    }
                    else {
                        legalOMovesLeft.add(false);
                    }
                    if(validateMoveO(row, col, row-1, col+1) == true) {
                        legalOMovesLeft.add(true);
                    }
                    else {
                        legalOMovesLeft.add(false);
                    }
                    if(validateMoveO(row, col, row+1, col-1) == true) {
                        legalOMovesLeft.add(true);
                    }
                    else {
                        legalOMovesLeft.add(false);
                    }
                    if(validateMoveO(row, col, row-1, col-1) == true) {
                        legalOMovesLeft.add(true);
                    }
                    else {
                        legalOMovesLeft.add(false);
                    }
                }
            }
        }
        return legalOMovesLeft;
    }

    /**
     * This method overrides the toString method from
     * the Object class and displays the checker board
     * as a string
     */
    public String toString(String[][] checkers) {
        String boardString = "";
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < checkersBoard.length; i++) {
            for(int j = 0; j < checkersBoard[0].length; j++) {
                builder.append("" + checkersBoard[i][j] + " ");
            }
            builder.append("\n");
            boardString = builder.toString();
        }
        return boardString;
    }
}
