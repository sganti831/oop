/*
Author: Sruti Ganti
Date: 3/18/21
Description: This program is written in C. It reads an input 
file that contain  characters which represent 4-bit HEX 
values. The program processes the input file, translates each
character to its respective binary pattern, and writes the 
result to the output file. 
Usage: This program does not contain a menu interface. 
Instead, it utilizes a command line utility. The usage 
syntax is as follows: <exe> <input file> <-a|-i> <output file>
example - hexArt info.txt -a art.txt
*/

#include<stdio.h>
#include<string.h>

const char * hexToArt(char hexChar);
char invertAsciiArt(char origAscii);

int main(int argc, char *argv[]) {
  FILE *fileRead;
  FILE *fileWrite;

  /*if less than 4 arguments are provided, 
    print error message to screen*/
  if(argc < 4) {
    printf("Too few arguments.\n");
  }

  /*if more than 4 arguments are provided,
    print error message to screen*/
  else if(argc > 4) {
    printf("Too many arguments. \n");
  }

  /*if user selected a, convert hex to ascii
    and execute the following code*/
  if(argv[2][1] == 'a') {
    fileRead = fopen(argv[1], "r");
    fileWrite = fopen(argv[3], "w");

    /*if the input file is empty, print error
      message to screen*/
    if(fileRead == NULL) {
      printf("Unable to read from the file. \n");
    }

    char line[255];
    int index;

    /*continue reading input file as long as lines 
      to read are left*/
    while(fgets(line, sizeof(line), fileRead) != NULL) {
      //accesses each character within line
      for(index = 0; index < strlen(line); index = index + 1){
	//writes converted character to output file
	fprintf(fileWrite, "%s", hexToArt(line[index]));
      }
      fprintf(fileWrite, "%s", "\n");
    }
  
    //close input and output files
    fclose(fileRead);
    fclose(fileWrite);
  }

  /*if user selected i, invert ascii art and execute
    the following code*/
  else if(argv[2][1] == 'i') {
    fileRead = fopen(argv[1], "r");
    fileWrite = fopen(argv[3], "w");

    /*if input file is empty, print error message
      to the screen*/
    if(fileRead == NULL) {
      printf("Unable to read from the file. \n");
    }

    char line[1020];
    int index;

    /*continue reading input file as long as there
      are lines left*/
    while(fgets(line, sizeof(line), fileRead)) {
      //accesses each character within line
      for(index = 0; index < strlen(line); index = index + 1) {
	//writes inverted character to output file
	fputc(invertAsciiArt(line[index]), fileWrite);
      }
      fprintf(fileWrite, "%s", "\n");
    }
  }
  /*if user entered option other than a or i, 
    display error message*/
  else {
    printf("Invalid input.\n");
  }

  return 0;
}

/*this method takes in a char and returns
  the respective binary pattern for the char 
  as a C-style string literal*/
const char* hexToArt(char hexChar) {
  //switch case for different values of hexChar
  switch(hexChar) {
  case '0':
    return "----";
    break;

  case '1':
    return "---#";
    break;

  case '2':
    return"--#-";
    break;

  case '3':
    return "--##";
    break;

  case '4':
    return "-#--";
    break;

  case '5':
    return "-#-#";
    break;

  case '6':
    return "-##-";
    break;

  case '7':
    return "-###";
    break;

  case '8':
    return "#---";
    break;

  case '9':
    return "#--#";
    break;

  case 'A':
  case 'a':
    return "#-#-";
    break;

  case 'B':
  case 'b':
    return "#-##";
    break;

  case 'C':
  case 'c':
    return "##--";
    break;

  case 'D':
  case 'd':
    return "##-#";
    break;

  case 'E':
  case 'e':
    return "###-";
    break;

  case 'F':
  case 'f':
    return "####";
    break;

  case '\n':
    return "";
    break;
    
    //error case if hexChar is not 0-9 or A-F
  default:
    return "eeee";
  }
 }

/*this method takes a 4-bit ascii char and inverts 
the dashes for hashtags and the hashtags for 
dashes. It returns the inverted char.*/
char invertAsciiArt(char origAscii) {
  if(origAscii == '-') {
    return '#';
  }
  else if(origAscii == '#') {
    return '-';
  }
  else if(origAscii == '\n') {
    return ' ';
  }
  //error case if origAscii is neither dash nor hashtag
  else {
    return 'E';
  }
}
