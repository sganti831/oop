/*
Author: Sruti Ganti
Date: 4/2/21
Description: This program is written in C++. It creates a
Sierpinski Carpet based on the number of layers determined by the user.
Usage: This program does not contain a menu interface. Instead, it utilizes
a command line utility. The usage syntax is as follows: <exe> <output file>
example - sierpinski carpet.txt
*/

//include libraries
#include <iostream>
#include <cmath>
#include <fstream>
using namespace std; 

//declare functions
char carpetValue(int row, int col);
char squareIter(int row, int col);
void drawCarpet(int numOfLayers, const char* fileName);

int main(int argc, char *argv[]) {
  //declare int to store number of layers
  int numOfLayers;

  //welcome user to Seirpinski Carpet program
  cout << "Welcome to the Sierpinski Carpet Fractal Maker!" << endl;
 
  //prompt user for number of layers in carpet
  cout << "Please enter the number of layers for the carpet.\n";
  cout << "The upper limit is 7: \n";
  cin >> numOfLayers;

  /*check if number of layers surpasses upper limit
    if it does, prompt user again*/
  while(numOfLayers > 7) {
    cout << "Please enter a valid number of layers.\n";
    cout << "The upper limit is 7: \n";
    cin >> numOfLayers;
  }

  //call Sierpinski function to print out carpet
  drawCarpet(numOfLayers, argv[1]);

  //end main function
  return 0;
}

/*This method takes in the current row and col of the 2d dynamically
allocated array and checks to see what value to store within the array.
It calculates the center of the current square and makes it empty, while
making the rest of the squares '#'.
 */
char carpetValue(int row, int col) {
  //declare char to store array value in
  char result = '#';

  //if size is zero, return '#'
  if(row == 0 && col == 0) {
    result = '#';
  }
  //otherwise, divide square into 9 pieces
  if(row > 0 || col > 0) {
    result = squareIter(row,col);
  }
  //returns the char value associated with result
  return result;
}

//function to iteratively divide square into 9 pieces
char squareIter(int row, int col) {
  char result = '#';
  //divide square into 9 pieces 
  do {
    if(row % 3 == 1 && col % 3 == 1) {
      result = ' ';
    }
    //divide row by 3
    row = row / 3;
    //divide col by 3
    col = col / 3;
  }while(row > 0 || col > 0);
  //return char associated with result 
  return result;
}

//function to draw Sierpinski carpet 
void  drawCarpet(int numOfLayers, const char* fileName) {
  //declare file to write to
  ofstream writeFile;

  //check if filename was provided as an argument
  if(fileName == NULL) {
    writeFile.open("carpet.txt", ios::out);
  }
  else {
    writeFile.open(fileName, ios::out);
  }

 //declare array dimensions
  int rows = pow(3, numOfLayers);
  int cols = pow(3, numOfLayers);

  //decare 2D dynamically allocated array
  char* sierpinskiCarp = new char[rows * cols];

  //write values to array
  for(int i = 0; i < rows; i++) {
    for(int j = 0; j < cols; j++) {
      /*the two for loops allow the array to be accessed at 
      specific row and column*/
      *(sierpinskiCarp + i * cols + j) = carpetValue(i,j);
    }
  }

  //if n is less than or equal to 3
  //print to screen AND write to file
  if(numOfLayers <= 3) {
    for(int i = 0; i < rows; i++) {
      for(int j = 0; j < cols; j++) {
	cout << *(sierpinskiCarp + i * cols + j);
	writeFile << *(sierpinskiCarp + i * cols + j);
      }
      cout << endl;
      writeFile << endl;
    }
  }
  //if n is greater than 3
  //ONLY write to file 
  else {
    for(int i = 0; i < rows; i++) {
      for(int j = 0; j < cols; j++) {
	writeFile << *(sierpinskiCarp + i * cols + j);
      }
      writeFile << endl;
    }
  }

  //close file
  writeFile.close();

  //release dynamically allocated array memory
  delete [] sierpinskiCarp;
}
