"""
Author: Sruti Ganti
Date: 3/19/23
Description: Contains code to support GUI interaction for application.
References: Throughout developing the following code, [2] was used as a reference.
Please view "File Encryption Tutorial (README.md)" for a list of references.
"""
import os
import tkinter
from tkinter import *
from tkinter import ttk, filedialog, messagebox

import cryptography
# import encryption library
from cryptography.fernet import Fernet

from encrypt_decrypt import EncryptDecrypt


class GUI():
    filepath = ""
    encrypt = EncryptDecrypt()
    def construct_GUI(self):
        # generate key for encryption
        encryptionKey = Fernet.generate_key()
        # string encryption key into a file
        with open('../encryptionkey.key', 'wb') as key:
            key.write(encryptionKey)
        # open encryption key
        with open('../encryptionkey.key', 'rb') as key:
            encryptionKey = key.read()

        # create root window
        root = Tk()
        root.title("File Encryption/Decryption")
        root.geometry('500x500')
        # create a frame to hold components
        frame = Frame(root, width=500, height=500)
        frame.grid(row = 0, column = 0, sticky="NW")
        # adding components to GUI
        greetLbl = Label(frame, text = "Welcome to File Encryption/Decryption!", font='Times 14')
        greetLbl.place(relx = 0.5, rely = 0.05, anchor=CENTER)
        fileBtn = Button(frame, text = "Select file", command=lambda : self.open_selected_file(text_widget),
                         font='Times 12', height=1,
                         width=44,
                         highlightcolor="orange",
                         highlightbackground="orange",
                         highlightthickness=2,
                         default='active')
        fileBtn.place(relx = 0.5, rely = 0.15, anchor=CENTER)
        text_widget = Text(frame, height=20, width=50)
        text_widget.place(relx = 0.5, rely=0.54, anchor=CENTER)
        scrollbar = Scrollbar(frame, orient=VERTICAL)
        text_widget.config(yscrollcommand=scrollbar.set)
        scrollbar.config(command=text_widget.yview)
        scrollbar.place(relx = 0.93, rely=0.54, anchor=CENTER)
        encBtn = Button(frame, text = "Encrypt", command=lambda : self.simple_encrypt(encryptionKey, text_widget),
                        font='Times 12',
                        width=18,
                        highlightcolor="orange",
                        highlightbackground="orange",
                        highlightthickness=2,
                        default='active')
        encBtn.place(relx=0.264, rely=0.93, anchor=CENTER)
        decBtn = Button(frame, text = "Decrypt", command=lambda : self.simple_decrypt(encryptionKey, text_widget),
                        font='Times 12',
                        width=18,
                        highlightcolor="orange",
                        highlightbackground="orange",
                        highlightthickness=2,
                        default='active')
        decBtn.place(relx=0.737, rely=0.93, anchor=CENTER)
        root.resizable(False, False)
        root.mainloop()

    def open_selected_file(self, text_widget):
        self.filepath = filedialog.askopenfilename(title="Select a File")
        self.write_file(text_widget, self.filepath)

    def simple_encrypt(self, encryptionKey, text_widget):
        try:
            if os.path.getsize(self.filepath) == 0:
                raise IOError
            if not self.filepath.lower().endswith('.txt'):
                raise ImportError
            self.encrypt.encrypt_file(encryptionKey, self.filepath)
            self.encrypt_not(encrypt=True)
            text_widget.delete("1.0", "end")
            self.write_file(text_widget, self.filepath)
        except IOError:
            messagebox.showinfo("ERROR", "Empty file can not be encrypted.")
        except ImportError:
            messagebox.showinfo("ERROR", "Invalid file type; can not be encrypted.")

    def simple_decrypt(self, encryptionKey, text_widget):
        try:
            if os.path.getsize(self.filepath) == 0:
                raise IOError
            if not self.filepath.lower().endswith('.txt'):
                raise ImportError
            self.encrypt.decrypt_file(encryptionKey, self.filepath)
            self.encrypt_not(encrypt=False)
            text_widget.delete("1.0", "end")
            self.write_file(text_widget, self.filepath)
        except IOError:
            messagebox.showinfo("ERROR", "Empty file can not be decrypted.")
        except ImportError:
            messagebox.showinfo("ERROR", "Invalid file type; can not be decrypted.")
        except cryptography.fernet.InvalidToken:
            messagebox.showinfo("ERROR", "Oops! Please encrypt the file before attempting to decrypt.")

    def encrypt_not(self, encrypt):
        if encrypt == True:
            messagebox.showinfo("Encryption Results", "Your file has been encrypted!")
        else:
            messagebox.showinfo("Decryption Results", "Your file has been decrypted!")

    def write_file(self, text_widget, filepath):
        try:
            if os.path.getsize(filepath) == 0:
                raise IOError
            if not self.filepath.lower().endswith('.txt'):
                raise ImportError
            text_widget.delete("1.0", "end")
            file_contents = open(filepath, "rb")
            data = file_contents.read()
            text_widget.insert(tkinter.END, data)
            file_contents.close()
        except IOError:
            messagebox.showinfo("WARNING", "File is empty. Please ensure file has contents.")
        except ImportError:
            messagebox.showinfo("WARNING", "Please ensure the selected file is a text file.")








