"""
Author: Sruti Ganti
Date: 3/19/23
Description: Contains logic for encrypting/decrypting a file.
References: Throughout developing the following code, [4] was used as a reference.
Please view "File Encryption Tutorial (README.md)" for a list of references.
"""
# import encryption library
from cryptography.fernet import Fernet

class EncryptDecrypt():
    # simple encryption
    def encrypt_file(self, encryptionKey, filePath):
        fernetObj = Fernet(encryptionKey)
        with open(filePath, 'rb') as file_to_encrypt:
            orig_data = file_to_encrypt.read()
        # encrypt file with encryption key
        encrypted_data = fernetObj.encrypt(orig_data)
        with open(filePath, 'wb') as encrypted_file:
            encrypted_file.write(encrypted_data)

    # simple decryption
    def decrypt_file(self, encryptionKey, filePath):
        fernetObj = Fernet(encryptionKey)
        # open encrypted file
        with open(filePath, 'rb') as encrypted_file:
            encrypted_data = encrypted_file.read()
        # decrypt file
        decrypted_data = fernetObj.decrypt(encrypted_data)
        # write decrypted data to file
        with open(filePath, 'wb') as decrypted_file:
            decrypted_file.write(decrypted_data)


