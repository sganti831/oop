"""
Author: Sruti Ganti
Date: 3/19/23
Description: Contains code to support CLI interaction for application.
"""
import os.path
import sys

import cryptography.fernet
# import encryption library
from cryptography.fernet import Fernet

from encrypt_decrypt import EncryptDecrypt

class CommandLine():
    def command_line_menu(self):
        encrypt = EncryptDecrypt()
        # generate key for encryption
        encryptionKey = Fernet.generate_key()
        # string encryption key into a file
        with open('../encryptionkey.key', 'wb') as key:
            key.write(encryptionKey)
        # open encryption key
        with open('../encryptionkey.key', 'rb') as key:
            encryptionKey = key.read()
        while True:
            print("Command Line Interface (CLI)\n-------------------------------------------------")
            try:
                commLineChoice = int(input("Please select an option from the following menu:\n"
                      "1. Encrypt a file.\n"
                      "2. Decrypt a file.\n"
                      "3. Quit the CLI (Command Line Interface).\n"))
                if commLineChoice < 1 or commLineChoice > 4:
                    print("Please select a VALID integer option.\n")
                if commLineChoice == 1:
                    try:
                        filePath = input("Please enter the path of the file you'd like to encrypt:\n")
                        if not filePath.lower().endswith('.txt'):
                            raise ImportError
                        if os.path.getsize(filePath) == 0:
                            raise IOError
                        encrypt.encrypt_file(encryptionKey, filePath)
                        print("Your file: %s has been encrypted!\n" % (filePath))
                    except FileNotFoundError:
                        print("Oops! That file can not be found. Please check the file path.\n")
                    except IOError:
                        print("File is empty. Please ensure file has contents before encrypting.\n")
                    except ImportError:
                        print("Please ensure the selected file is a text file.\n")
                elif commLineChoice == 2:
                    try:
                        filePath = input("Please enter the path of the file you'd like to decrypt:\n")
                        if not filePath.lower().endswith('.txt'):
                            raise ImportError
                        if os.path.getsize(filePath) == 0:
                            raise IOError
                        encrypt.decrypt_file(encryptionKey, filePath)
                        print("Your file: %s has been decrypted!\n" % (filePath))
                    except FileNotFoundError:
                        print("Oops! That file can not be found. Please check the file path.\n")
                    except cryptography.fernet.InvalidToken:
                        print("Oops! Please encrypt the file before attempting to decrypt.\n")
                    except ImportError:
                        print("Please ensure the selected file is a text file.\n")
                elif commLineChoice == 3:
                    print("Exiting CLI...\n")
                    break
            except ValueError:
                print("Please select a valid option from the menu.\n")
            except IOError:
                print("File is empty. Please ensure file has contents before decrypting.\n")

