"""
Author: Sruti Ganti
Date: 3/19/23
Description: Main class for encryption/decryption application.
"""
import sys

from command_line import CommandLine
from gui import GUI


class FileEncryption():
    commandLine = CommandLine()
    gui = GUI()

    if __name__ == '__main__':
        userChoice = -1
        while True:
            try:
                userChoice = int(input("Welcome to File Encryption!\n"
                       "Please select an option from the menu:\n"
                       "1. Continue in CLI (Command Line Interface)\n"
                       "2. Continue in GUI (Graphical User Interface)\n"
                       "3. Physics Quotes and Jokes\n"
                       "4. Quit\n"))
            except:
                print("Please select an INTEGER option.")
            if userChoice < 1 or userChoice > 4:
                print("Please select a VALID integer option.\n")
            elif userChoice == 1:
                commandLine.command_line_menu()
            elif userChoice == 2:
                gui.construct_GUI()
            elif userChoice == 3:
                dict = {
                    "1": "'Mathematics is not about numbers, eqautions,\n"
                         "computations, or algorithms: it is about understanding.\n"
                         "- Srinivasa Ramanujan'\n",
                    "2": "What is the most terrifying word in nucleaur physics?\n"
                         "Oops.\n",
                    "3": "'An explosion in space makes no sound, as there is no\n"
                         "air to transmit the sound waves.'\n"
                         "- Kip Thorne\n",
                    "4": "'I think I can safely say that nobody understands\n"
                         "quantum mechanics.'\n"
                         "- Richard Feynman\n",
                    "5": "What did one photon say to another?\n"
                         "I am sick and tired of your interference!\n",
                }
                try:
                    physicsChoice = input("Please enter a random number between 1 and 5 (inclusive).\n")
                    if int(physicsChoice) < 1 or int(physicsChoice) > 5:
                        print("Please enter a VALID integer option.\n")

                    if physicsChoice.__eq__("1") or physicsChoice.__eq__("3") or physicsChoice.__eq__("4"):
                        print("Here is your Physics quote:\n", dict.get(physicsChoice))
                    elif physicsChoice.__eq__("2") or physicsChoice.__eq__("5"):
                        print("Here is your Physics joke:\n", dict.get(physicsChoice))
                except:
                    print("Please select a valid option from the menu.\n")
            elif userChoice == 4:
                print("Thank you for using File Encryption!")
                sys.exit(1)