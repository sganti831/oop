import cryptography as cryptography
from setuptools import setup

setup(
    name='file_encryption_package',
    version='0.1',
    description='Setup script for file_encryption package',
    author='Sruti Ganti',
    packages=['file_encryption_package'],
    install_requires=[
        cryptography
    ],
)