# File-Encryption
This project is an Honors Contract for SER 335: Secure Systems (Spring 2023 Semester).

## File Encryption Tutorial

### I. Project Overview
The purpose of this project is two-fold: 
1. To develop file encryption software using Python that provides users with a CLI (Command Line Interface) or a GUI (Graphical User Interface) to encrypt/decrypt the contents of a file. The core of this project is the encryption algorithm being deployed to encrypt/decrypt the contents of the specified files. 
2. To chronicle the development process and provide a tutorial for others interested in carrying out a similar project, or looking for practice with version control, Python, and encryption algorithms. 

### II. Software Requirements:
1. The software shall encrypt/decrypt text files using a pre-existing encryption algorithm.
2. The software shall be written in Python.
3. The software shall provide the user with a CLI and GUI. 
4. The software shall be equipped with a step by step tutorial. 
5. The software shall save/update the contents of the file after encryption and/or decryption. 
6. The software, specifically the GUI, shall display the contents of the file after encryption and/or decryption.

### III. Tutorial Learning Outcomes
The learning outcomes for this project are as follows:
1. Develop a basic understanding of version control.
2. Understand the purpose of/how to create a setup.py file.
3. Parse/read input from the command line. 
4. Implement a simple GUI in Python.
5. Understand the basics of encryption and how encryption algorithms are developed. 
6. Deploy existing encryption algorithms.
7. Error catching/handling errors gracefully.    

### IV. Using the Software

#### A. Environment Setup
Step 1: Confirm you have Python on your machine by running the `python --version` command in your terminal. If an error is returned, download the latest version of Python for your operating system here: https://www.python.org/downloads/. 

Step 2: Clone the ser335-File-Encryption GitHub repository using the `git clone` command. 

Step 3: In order to set up your environment to successfully run the software, please use the command  `pip install .`
*Ensure you are in the same directory as the setup.py file before executing this command*. This command installs the latest versions of the packages, and their dependencies, specified in the setup.py file. 

**Note:** If `pip install .` produces errors, please directly download cryptography using the following command: `pip install cryptography`. 

#### B. Executing the Source Code
Step 1: Run the main method within the main.py file from an IDE, or the command line. The remaining logic executes based on the menu options selected by the user. There are no other classes, or methods that need to be run separately. 

#### C. Encryption Algorithms Overview
Please note the contents of this section are derived from [1]. 

##### 1. Cryptography Introduction
Cryptography is the science of securing data that is in transit, or at rest. To clarify, data in transit refers to data in motion; e.g., when you send a Whatsapp message to a family member, the data in transit is the contents of your message. Data at rest refers to data that is not in motion; e.g. data stored in an external hard drive. At a high level, encryption is the process of converting data into a code in order to protect privacy and prevent unauthorized access.      

##### 2. Fernet Class Overview
This software utilizes the Fernet class within the Python cryptography package to encrypt/decrypt the contents of a file. Fernet is an implementation of symmetric key encryption, a type of encryption where the **same** secret key is used to encrypt and decrypt data. Additionally, the Fernet class ensures that encrypted data can not be manipulated, or read without the secret key. 

##### 3. Detailed Encryption Implementation 
The following is a detailed description of the inner-workings of the Fernet class, specifically how encryption and decryption are applied. The main components utilized for encryption/decryption in the Fernet class are:
* Inputs: 
A set of inputs (the data that needs to be encrypted, a unique key to encrypt/decrypt the data, and a timestamp) are used to generate a fernet token. 
  
* 128-bit AES:
Advanced Encryption Standards (AES) is a block cipher that is used to encrypt data. Data is split into fixed-length strings and added to blocks of size 128 bits. An initialization vector is selected at random to configure the first block for each token. The AES block cipher is limited to encrypting one block at a time. As a result, Cipher Block Chaining (CBC) is utilized alongside AES to encrypt multiple blocks of data in tandem. 

* Padding:
Padding is added to the 128 bit sized blocks for two main reasons:
  1. In order to encapsulate the actual length of the data. This increases the overall security of the AES algorithm.
  2. To ensure that the data fits the pre-set size. 

* HMAC (SHA256):
Hash-based Message Authentication Code (HMAC) makes use of a cryptographic key accompanied by a hash function derived from the SHA256 hashing algorithm. HMAC provides the distributed system (client and server) with the unique and secure private key. 

* Token: 
A fernet token consists of the encrypted data, along with the unique and secure key, and timestamp. The fernet token servers to provide an additional level of security and authentication. 

###### I. Fernet Token Fields
* Version: Specifies fernet version that will be utilized for encryption/decryption. 

* Timestamp: Represented as a 65 bit unsigned big-endian integer, and quantified as the number of seconds between January 1, 1970 (UTC) to the date of the token's initiation.  

* Initialization Vector: Unique 128 bit vector appended to the first block of each token during AES encryption/decryption. The initilization vector is randomly generated using the os.urandom() function.  

* Ciphertext (encrypted data): Encrypted data will be padded to a size of 128 bits within each block. 

* HMAC: The version, timestamp, initialization vectore, and ciphertext will be authenticated with HMAC/SHA256 hashing algorithm. 

###### II. Fernet Encryption
In order to encrypt the contents of the specified file, the following steps are executed within the Fernet encrypt method:
  1. The current timestamp is recorded.
  2. The os.urandom() function is used to generate a unique initialization vector.
  3. The ciphertext is then constructed by first padding the plain text to 128 bits as outlined in the previous section under "Padding". Then, the padded plain text is encrypted via AES with CBC. A user defined key and the previosly generated initialization vector are used during the AES encryption process. 
  4. The HMAC is then computed for the specified version, timestamp, intialization vector, and encrypted data. 
  5. The token is then encoded using base64url specification. Refer to the "VII. Supplementary Resources" section for more information regarding the base64url specification.  

###### III. Fernet Decryption
In order to decrypt the contents of the specified file, the following steps are executed withi the Fernet decrypt method:
  1. Fernet token is decoded using base64url specification. Refer to the "VII. Supplementary Resources" section for more information regarding the base64url specification.  
  2. The token is checked to ensure it is not outdates, and the first byte of the token is verified (should be 0x80). 
  3. The HMAC is re-computed and validated against the previously generated HMAC for encryption.
  4. The ciphertext (encrypted data) constructed during encryption is then decrypted using the initialization vector and user defined key.
  5. The decrypted data is then unpadded and written to the specified file as plain text. 

##### 4. Fernet Limitations 
Please note the contents of this section are derived from [5]. 

While the Fernet class offers an encryption algorithm that is simple to use and easy to deploy, the algorithm has its limitations. Regarding computational efficiency, CBC is a sequential algorithm; as a result, the algorithm is relatively slow. Furthermore, the output of the previous block is required as input for the next block. Additionally, the entire contents of the file must be able to fit within the available memory when using Fernet. Thus, Fernet is not a good fit for encrypting large files that consist of lots of data. 

### V. Step by Step Tutorial

#### A. Version Control
Throughout the duration of this project, using version control is highly recommended. If your host operating system is Windows, you may download Git here: https://gitforwindows.org/. For those working on a Linux system, follow the steps outlined in this document: https://git-scm.com/download/linux. 

##### 1. Setting up Your Repository
Step 1: If this is your first time using Git/setting up a repository, reference the "Supplementary Resources" section below to become familiarized with version control, Git, and setting up a repository. Set up and clone your GitHub repository. 

Step 2: Set up your .gitignore file. Refer to the list of resources below if you are unfamiliar with .gitignore files. For reference, view the .gitignore file in the given repository (ser335-File-Encryption). 

Step 3: Create a setup.py file. The purpose of a setup.py file is to ensure others can easily build and use your application. For reference, view the setup.py file in the given repository and also view the list of resources below. Keep in mind you can add to your setup.py file as you continue developing, since the requirements/dependenices your software requires may not be known upfront.      

#### B. Command Line Interface

##### 1. Greeting the User
Step 1: Upon opening your application, the user should be greeted with a simple menu. Ensure your menu is simple and easy to follow.  

**Challenge 1:** Add your own fun option for the user! You can use option 3 ("Physics Quotes and Jokes") in the source code as an example, but be sure to come up with your own unique option.

##### 2. Encryption via the Command Line
Step 1: Provide the user with a menu for the following options: 
1. Encrypt a file.
2. Decrypt a file.

*Your menu is not limited to the options above; you may add additional functionality as long as the two options above are included.*

Tip: When implementing your menu, consider whether the command line menu should persist, or if the user should signal in order to exit the CLI. In other words, should the command line options keep repeating, or should the user automatically exit from the CLI upon encrypting a file? 

Step 2: Tie the encryption/decryption logic into the menu. One option is to create a class that consists of encryption/decryption logic only. Then, you can make method calls to the encryption/decryption class based on the CLI menu option. Refer to the source code for implementation guidance (specifically the command_line.py class).  

#### C. Graphical User Interface
Step 1: Before jumping into implementation, take some time to design your GUI. Consider the features you'd like to include and create a paper prototype of your desired end result.

##### 1. Greeting the User
Step 1: Be sure to include a label, or some type of text description that greets the user. 

##### 2. GUI Implementation (Front-End)

Step 2: Implement the front-end of your GUI. Based on the design you developed in Step 1, add label and button components to your GUI. Refer to the "VII. Supplementary Resources" section below for references on creating a GUI in Python. 

Tip: For Step 2, isolate the functionality and focus purely on the front-end of your GUI. This means set up your GUI first by adding all the necessary labels, buttons, text widgets, etc. Once you are satisfied with the design of your GUI, then move on to the back-end. 

##### 3. Encryption via the GUI
Step 3: Implement the back-end of your GUI. Tie the encryption/decryption logic into the menu. Consider what should happen when the user clicks a specific button. Refer to the "VII. Supplementary Resources" section below for references on creating event listeners/adding button click actions. For implementation guidance, refer to the source code (specifically the gui.py class).      

Tip: If your button click code is being executed before the button is actually clicked, investigate your button click logic. What exactly are you passing to your function? Hint: research lambda expressions in Python. 

#### D. Using Existing Encryption Algorithms
Step 1: Research the various encryption algorithms offered by the Python cryptography package. Select one algorithm, and implement the encryption and decryption functionality. Do not use the Fernet class, or copy paste the given source code. Refer to the "VII. Supplementary Resources" section below for a list of various encryption algorithms within the cryptography package. For implementation guidance, refer to the source code (specifically the encrypt_decrypt.py class). 

Tip: Implement the encryption and decryption functionality as method calls in order to easily integrate the encryption/decryption logic into the menu options. 

Step 2: Understand, at a high level, the functionality within the Python cryptography package. In order to provide adequate responses to the questions below, thoroughly research the Fernet class within the cryptography package. 

**Challenge 2:**
1. What is symmetrical encryption?
2. What is the difference between symmetrical encryption and asymmetrical encryption? Provide an example for each encryption type. 
3. Explain the *line by line* execution of the encryption functions (simple encryption/decryption) within the EncryptDecrypt class. 

Compare your answers against the solutions in the "VI. Challenge Solutions" section. 

#### E. Analyzing a More Complex Encryption Algorithm 
Step 1: Research various encryption algorithms used for data privacy and security. Find an encryption algorithm that seems interesting and a bit complex, and analyze the algorithm. Do not use Caesar Cipher, or a very simple algorithm - challenge yourself! 

**Challenge 3:** At a high level, describe the theory/steps behind the encryption algorithm you chose. For example, if using the Caesar Cipher, the steps would be the following:
1. Read and record each letter within the plain text file.
2. Replace each letter with the corresponding letter three shifts down in the alphabet (e.g. Replace 'A' with 'D').

Include the encryption algorithm's limitations (if any) and discuss scenarios for which the algorithm should and should not be deployed. In your response, include interesting details about the algorithm. 

#### F. Error Handling
Step 1: For each module within your source code (CLI, GUI, encryption/decryption), consider what could go wrong. E.g., what if the user enters a string instead of integer option for the menu? What if the file the user selects does not exist? 

Tip: Think creatively and from a user's perspective. It may be difficult to detach yourself from the code you developed. However, proceed with the intention of wanting to break your own code. 

Step 2: Now implement solutions for what could go wrong. E.g., if a user enters a string instead of an integer option for the menu, how should this be handled? Should the user be notified of the error? Should the program quit? 

Tip: Use exception handling as you see fit for errors. Refer to the "VII. Supplementary Resources" section for references regarding exception handling in Python. 

#### G. Ultimate Challenge: Implement Your Own Encryption Algorithm (*Optional*)
Step 1: Please note this challenge is *optional*. Put your encryption skills to the test and develop your own unique encryption algorithm! Under the "VII. Supplementary Resources" section, you will find a few resources to help get you started. 

Step 2: If you'd like, you may extend from the source code in the given repository. The source code utilizes a form of symmetric encryption (Fernet class within the cryptography package) to encrypt/decrypt the contents of a file. See if you can implement an asymmetric encryption algorithm. Refer to the "VII. Supplementary Resources" section for guidance. 

Tip: Implement your encryption algorithm as a separate class that consists of the unique encryption/decryption logic.

Step 3: Be sure to include a brief description in your README.md file explaining the algorithm you developed and implemented. Highlight the features that make it unique and include a description of the algorithm's execution. If you decided to implement asymmetric encryption, provide a brief description of the difference between the algorithm you implemented and the symmetric algorithm deployed in the source code.  

### VI. Challenge Solutions
This section consists of solutions to closed-ended challenge questions. Solutions to open-ended challenge questions, such as implementing a specific feature, are not included in this section. Be sure to complete the challenge questions before viewing the solutions! 

#### Challenge 1
Requirements:
* Implemented option is unique and fun in nature. 
* Implemented option extends main menu.
* Implemented option errors are handled gracefully; system does not crash upon entering incorrect input. 

#### Challenge 2
Please note the contents of this section are derived from [3]. 

1. What is symmetric encryption?
 
Answer: Symmetric encryption is a type of encryption where only one key (the same key) is used to encrypt and decrypt data. Since the same key is being used to encrypt and decrypt the data, symmetric key encryption is not the most secure form of encryption. 

2. What is asymmetric encryption?

Answer: Asymmetric encryption is a type of encryption that utilizes a pair of keys (one public key and one private key) to encrypt and decrypt data. The sender can pull the recipient's public key to direct the data towards a specific person. However, only the recipient has the private key in order to decrypt the data.

HTTPS, specifically the transport layer security (TLS)/secure sockets layer (SSL) protocol utilizes asymmetric encryption.  

3. What is the difference between symmetric encryption and asymmetric encryption? Provide an example for each encryption type.
  
Answer: Symmetric encryption relies on using one key to encrypt and decrypt data, whereas asymmetric encryption relies on two keys (one public key and one private key) to encrypt and decrypt data. Symmetric encryption is less secure than asymmetric encryption, since the same key is used for encrypting and decrypting. Furthermore, if the secret key is lost, then the encrypted data can not be decrypted (data is also lost). However, symmetric encryption is more computationally efficient than asymmetric encryption and less resource intensive. 

Asymmetric encryption is more secure since two keys are utilized. However, there is a trade-off between security and computational efficiency. Since two separate keys are used, asymmetric encryption is less computationally efficient and more resource intensive than symmetric encryption. Unlike symmetric encryption, if a key is lost the data is not necessarily lost (unless both keys are lost).   

Examples of symmetric encryption include AES, RC4, DES. Examples of asymmetric encryption include TLS and SSL protocols. A real world example of asymmetric encryption is the process of sending emails. Others are able to access your email domain, but only you are able to login to your account in order to read your emails. For an explanation of the aforementioned algorithms and protocols, please refer to the "VII. Supplementary Resources" section.  

5. Explain the *line by line* execution of the encryption functions (simple encryption/decryption) within the EncryptDecrypt class.
 
Answer: 
encrypt_file
   1. The secret key is passed in order to encrypt the file. 
   2. The specified file is opened and parsed. 
   3. The file contents are then encrypted.
   4. The encrypted data is then written to the specified file.

*For a detailed explanation regarding the implementation behind the encryption, please refer to the "C. Encryption Algorithms Overview" section.*

decrypt_file
   1. The secret key is passed in order to decrypt the file.
   2. The specified file is opened and the encrypted contents are parsed. 
   3. The encrypted file contents are then decrypted.
   4. The decrypted data is then written to the specified file.

*For a detailed explanation regarding the implementation behind the decryption, please refer to the "C. Encryption Algorithms Overview" section.*

#### Challenge 3
Requirements:
* Selected algorithm is complex in nature
* Selected algorithm is *not* the Caesar Cipher or a very simple algorithm
* Theory/steps behin encryption algorithm are included
* Encryption algorithm limitations and scenarios are discussed 

#### Ultimate Challenge 
Requirements:
* Encryption algorithm is unique
* Decription outlining the uniqueness of the algorithm is present 
* If implementing asymmetric algorithm, brief comparison between symmetric algorithm and asymmetric algorithm is included. 

### VII. Supplementary Resources

#### A. Version Control and GitHub
* Understanding version control basics: https://about.gitlab.com/topics/version-control/ 
* Git introduction: https://www.atlassian.com/git 
* Git commands/documentation: https://git-scm.com/docs/gittutorial 
* Setting up a GitHub repository: https://product.hubspot.com/blog/git-and-github-tutorial-for-beginners 
* .gitignore files: https://git-scm.com/docs/gitignore#:~:text=A%20gitignore%20file%20specifies%20intentionally%20untracked%20files%20that%20Git%20should%20ignore. 

#### B. Python Configuration
* setup.py: https://www.geeksforgeeks.org/what-is-setup-py-in-python/ 

#### C. Creating a GUI in Python
* Basics: https://www.tutorialspoint.com/python/python_gui_programming.htm
* Event handling: https://pythonprogramming.net/tkinter-tutorial-python-3-event-handling/ 
* Button clicks: https://www.tutorialspoint.com/how-to-handle-a-button-click-event-in-tkinter 

#### D. Cryptography Encryption Algorithms
* List of algorithms in cryptography package: https://www.tutorialspoint.com/cryptography_with_python/cryptography_with_python_quick_guide.htm
* List of algorithms in cryptography package: https://legacy.python.org/workshops/1995-05/pct.html 

#### E. Exception Handling in Python
* Catching exceptions: https://docs.python.org/3/tutorial/errors.html 
* Raising exceptions: https://www.w3schools.com/python/gloss_python_raise.asp 
* Creating custom exceptions: https://www.programiz.com/python-programming/user-defined-exception 

#### F. Implementing Your Own Encryption Algorithm 
* Getting started: https://medium.com/analytics-vidhya/build-your-encryption-program-in-python-in-8-min-with-this-mini-project-code-snippets-dacdf53ee581
* Getting started: https://medium.com/swlh/create-your-own-custom-encryption-in-python-4345cc23512b
* Basic list of ciphers: https://www.educba.com/types-of-cipher/

##### A. Asymmetric Encryption
* Getting started: https://towardsdatascience.com/asymmetric-encrypting-of-sensitive-data-in-memory-python-e20fdebc521c 
* Getting started: https://medium.com/@ashiqgiga07/asymmetric-cryptography-with-python-5eed86772731

#### G. Symmetric/Asymmetric Examples
* AES: techtarget.com/searchsecurity/definition/Advanced-Encryption-Standard 
* RC4: https://www.geeksforgeeks.org/what-is-rc4-encryption/ 
* DES: https://en.wikipedia.org/wiki/Data_Encryption_Standard
* TSL/SSL protocol: https://www.tutorialsteacher.com/https/how-ssl-works
* Email protocol: https://security.berkeley.edu/education-awareness/email-encryption-guide 

#### H. URL Specifications
* base64url specification: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URLs

### VIII. References
[1] "A Deep Dive into Fernet Module in Python," ed: Pythonista Planet.

[2] "Python - GUI Programming (Tkinter)," ed: TutorialsPoint.

[3] Arnaud, "Symmetric vs. Asymmetric Encryption: What's the Difference?," ed: Mailfence, 2023.

[4] K. Chippa, "Encrypt and Decrypt Files Using Python," ed: Geeks for Geeks, 2022.

[5] J. Lake, "What is Fernet and When Should You Use It?," ed: Comparitech, 2022.
