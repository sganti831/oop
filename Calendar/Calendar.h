/*
Author: Sruti Ganti
Date: 4/22/21 
Description: This header file is responsible for all 
class information relevant to the Calendar.cpp class
*/

#ifndef CALENDAR_H
#define CALENDAR_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "Day.h"
using namespace std;

class Calendar {
 private:
  string calTitle;
  int numOfDays;
  string startDay;
  vector<Day*> daysList;

 public: 
  Calendar();
  Calendar(string title, int numDays, string beginDay);
  void displayAgenda(int day);
  void addToDay(int dayView, int choice, int begHr, int begM, int endHr,
		int endM, string text);
  void removeFromDay(int dayView, int deleteErrand);
  void writeToFile(string fileName);
  void toString();
};

#endif //CALENDAR_H

