/*
Author: Sruti Ganti
Date: 4/19/21
Description: This is the header file containing all class
information for the Reminder.cpp file.
*/

#ifndef REMINDER_H
#define REMINDER_H

#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Reminder {
protected:
  string reminder;

public:
  Reminder(string text);
  virtual void toString();
};

#endif // REMINDER_H
