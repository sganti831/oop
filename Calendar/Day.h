/*
Author: Sruti Ganti
Date: 4/21/21
Description: This is the header file that contains all
class information for the Day.cpp class.
*/

#ifndef DAY_H
#define DAY_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "Reminder.h"
#include "Appointment.h"
using namespace std;

class Day {
private:
  int dayNum;
  string dayOfWeek;
  ofstream calendarFile;
  vector<Reminder*> reminderList;

private:
  string numErrands();

public:
  Day(int num, string day);
  void addErrand(int choice, int begHr, int begM, int endHr, 
		 int endM, string text);
  void removeErrand(int deleteErrand);
  void dayOverview();
  void toFile(ofstream& calFile);
  void toString();
  string getNext(string current);
};

#endif //DAY_H
