/*
Author: Sruti Ganti
Date: 4/22/21
Description: This class represents a Calendar. Errands can be 
added to specific days of the calendar, and daily agenda's are
also provided.
*/

//include libraries
#include <iostream>
#include <string>
#include <vector>
#include "Calendar.h"
using namespace std;

//default class constructor
//intializes default instance variables and builds Day* vector 
Calendar::Calendar() {
  calTitle = "Calendar";
  numOfDays = 30;
  startDay = "Monday";
  for(int i = 1; i < numOfDays + 1; i++) {
    Day* day = new Day(i, startDay);
    daysList.push_back(day);
    startDay = day->getNext(startDay);
  }
}

//class constructor that intializes instance variables with user values
//builds Day* vector
Calendar::Calendar(string title, int numDays, string startDay) {
  calTitle = title;
  numOfDays = numDays;
  startDay = startDay;
  for(int i = 1; i < numOfDays + 1; i++) {
    Day* day = new Day(i, startDay);
    daysList.push_back(day);
    startDay = day->getNext(startDay);
  }
}

//this method displays an in-depth agenda for a specific day
void Calendar::displayAgenda(int day) {
  for(int i = 0; i < daysList.size(); i++) {
    //find day
    if(i == day - 1) {
      //print out day's contents
      daysList[i]->toString();
    }
  }
}

//this method adds an errand to the specified day
void Calendar::addToDay(int dayView, int choice, int begHr, int begM,
			     int endHr, int endM, string text) {
  for(int i = 0; i < daysList.size(); i++) {
    //find the day
    if(i == dayView - 1) {
      //add errand to the day
      daysList[i]->addErrand(choice, begHr, begM, endHr, endM, text);
    }
  }
}

//this method removes and errand from the specified day
void Calendar::removeFromDay(int dayView, int deleteErrand) {
  for(int i = 0; i < daysList.size(); i++) {
    //find the day
    if(i == dayView - 1) {
      //remove the errand
      daysList[i]->removeErrand(deleteErrand);
    }
  }
}

//this method writes the calendar to a file
void Calendar::writeToFile(string fileName) {
  //create file to write to
  ofstream calFile;
  //open file
  calFile.open(fileName);
  //add calendar title to file
  calFile << calTitle << endl;
  //iterate through vector and add contents to file
  for(vector<Day*>::iterator itr = daysList.begin(); itr != daysList.end();
      ++itr) {
    (*itr)->toFile(calFile);
  }
  //close the file
  calFile.close();
}

//this method prints out the calendar's contents
void Calendar::toString() {
  //print out calendar title
  cout << calTitle << endl;
  //iterate through vector 
  for(vector<Day*>::iterator itr = daysList.begin(); itr != daysList.end();
      ++itr) {
    //print out each day
    (*itr)->dayOverview();
  }
}
