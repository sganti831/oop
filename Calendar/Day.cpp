/*
Author: Sruti Ganti
Date:
Description: This class represents a day in the calendar. Errands
can be either added or removed to the calendar, and the contents
of a day can be displayed to the user.
*/

//include libraries
#include <iostream>
#include <string>
#include <vector>
#include "Day.h"
using namespace std;

//class constructor that initializes day
Day::Day(int num, string day) {
  dayNum = num;
  dayOfWeek = day;
}

//private method that returns the number of errands per day
string Day::numErrands() {
  //stores the number of errands as string
  string result;
  //if list is empty, return no
  if(reminderList.empty()) {
    result = "No";
  }
  //otherwise return number of items in list
  else {
    result = to_string(reminderList.size());
  }
  return result;
 }

//this method adds an errand to the day
void Day::addErrand(int choice, int begHr, int begM, int endHr, 
		    int endM, string text) {
  //if the user selected a reminder
  if(choice == 1) {
    //add new reminder
    Reminder* reminder = new Reminder(text);
    reminderList.push_back(reminder);
  }
  //if the user selected appointment
  else if(choice == 2) {
    //add new appointment
    Reminder* appointment = new Appointment(begHr, begM, endHr, endM, text);
    reminderList.push_back(appointment);
  }
  //if the user selected neither reminder nor appt
  else {
    //display error message
    cout << "Invalid option" << endl;
  }
}

//this method removes an errand from the specified day
void Day::removeErrand(int deleteErrand) {
  for(int i = 0; i < reminderList.size(); i++) {
    //find the day
    if(i == deleteErrand - 1) {
      cout << "Deleted - ";
      reminderList[i]->toString();
      //delete the errand
      reminderList.erase(next(begin(reminderList), + i));
    }
  }
}

//this method takes in a current day, and returns the next day
string Day::getNext(string current) {
  if(current == "Sunday") {
    return "Monday";
  }
  else if(current == "Monday") {
    return "Tuesday";
  }
  else if(current == "Tuesday") {
    return "Wednesday";
  }
  else if (current == "Wednesday") {
    return "Thursday";
  }
  else if(current == "Thursday") {
    return "Friday";
  }
  else if (current == "Friday") {
    return "Saturday";
  }
  else if (current == "Saturday") {
    return "Sunday";
  }
  return " ";
}

//this day returns an overview of a specific day
void Day::dayOverview() {
  cout << dayNum << " - " << dayOfWeek << ": " << numErrands() << " Errands"
       << endl;
}

//this method writes the current day to a file
void Day::toFile(ofstream& calFile) {
  calFile << dayNum << " - " << dayOfWeek << ": " << numErrands() << " Errands"
	  << endl;
}

//this method returns an in-depth view of the day
void Day::toString() {
  cout << dayNum << " - " << dayOfWeek << ":" << endl;
  //if list is empty
  if(reminderList.empty()) {
    cout << "No Errands Today" << endl;
  }
  else {
    int counter = 1;
    //iterate through vector and print contents
    for(vector<Reminder*>::iterator itr = reminderList.begin();
	itr != reminderList.end(); ++itr) {
      cout << counter << ". ";
      (*itr)->toString();
      counter++;
    }
  }
}

