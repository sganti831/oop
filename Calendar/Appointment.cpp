/*
Author: Sruti Ganti
Date: 4/20/21
Description: This class represents an appointment that
can either be added or removed from the calendar. This class 
inherits/is derived from the Reminder.cpp class.
*/

//include libraries
#include<iostream>
#include <string>
#include <vector>
#include "Appointment.h"
using namespace std;

//Class constuctor that initalized the appointment
Appointment::Appointment(int begHr, int begM, int endHr, int endM, string text) : Reminder(text) {
    beginHour = begHr;
    beginMin = begM;
    endHour = endHr;
    endMin = endM;
  }

//this method prints the contents of the appointment
void Appointment::toString() {
  cout << "Appointment: " << beginHour << ":" << beginMin <<" - " << endHour <<  ":" << endMin << " - " << reminder << endl;
  }

