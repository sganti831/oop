/*
Author: Sruti Ganti
Date: 4/22/21
Description: This class interacts with the user to 
create and display a calendar.
*/

//include libraries
#include <iostream>
#include <string>
#include <vector>
#include "Reminder.h"
#include "Appointment.h"
#include "Day.h"
#include "Calendar.h"
using namespace std;

int main() {
  //variable to store user's choice
  int userChoice;
  cout << "Welcome to Build Your Own Calendar! \n" << endl;
  cout << "Please choose an option from the menu:" << endl;
  //display options to user
  cout << "1. Create a new calendar" << endl << "2. Quit" << endl;
  cin >> userChoice;
  //clear cin
  cin.ignore();

  //switch case to handle different user choices
  switch(userChoice) {
  case 1: {
    char userCalChoice;
    //ask user which type of calendar
    cout << "Would you like to create a  default calendar or  personalized?\n"
	 << "Enter 'D' for default or 'P' for personalized." << endl;
    //store the type of calendar
    cin >> userCalChoice;
    //clear cin
    cin.ignore();
    
    //if user choice default calendar
    if(userCalChoice == 'D') {
     cout << "Making your default calendar..." << endl;
     //call default calendar constructor
     Calendar cal;
     cout << "Default calendar successfully built.\n" << endl;
     int userSel;
     //display calendar options to the user
     do{
       cout << "Menu:" << endl << "1. Display calendar" << endl <<
	 "2. Display a day in-depth"
	    << endl << "3. Add an errand"  << endl << "4. Delete an errand" << endl << "5. Save calendar to a file" << endl << "6. Exit" << endl;
       //store the user's choice 
       cin >> userSel;
       //clear cin
       cin.ignore();

       //switch case to handle the user's choice
       switch(userSel) {
       case 1: {
	 //display calendar contents
	 cal.toString();
       }
	 break;

       case 2: {
	 int viewDay;
	 //display calendar contents
	 cal.toString();
	 //ask user for specific day
	 cout << "Which day would you like to view?\n"
	   "Please enter as number - e.g. '1'" << endl;
	 //store user's input
	 cin >> viewDay;
	 //clear cin
	 cin.ignore();
	 //display specific day's agenda
	 cal.displayAgenda(viewDay);
       }
	 break;

       case 3: {
	 int viewDay;
	 int errandType;
	 int userSel;
	 //display calendar contents
	 cal.toString();
	 cout << "Which day would you like to add an errand to?" << endl
	      << "Please enter as number - e.g. '1'" << endl;
	 //store the day the user selected
	 cin >> viewDay;
	 //clear cin
	 cin.ignore();
	 //dispay menu of choices to user
	 cout << "What kind of errand would you like to add?" << endl
	      << "1. Reminder" << endl << "2. Appointment" << endl <<
	   "3. Return to main menu" << endl;
	 //store user selection
	 cin >> userSel;
	 //clear cin
	 cin.ignore();

	 //switch case depending on user input
	 switch(userSel) {
	 case 1: {
	   string remText;
	   //indicate reminder is being added
	   errandType = 1;
	   //store reminder text
	   cout << "Enter reminder text:";
	   getline(cin,remText);
	   //add reminder to the specific day
	   cal.addToDay(viewDay,errandType, 0,0,0,0,remText);
	   //display success message 
	   cout << "Reminder was successfully added." << endl;
	 }
	   break;

	 case 2: {
	   int begHour;
	   int begMin;
	   int endHour;
	   int endMin;
	   string apptText;
	   //indicate user chose appointment
	   errandType = 2;
	   //store user's choices for appointment info
	   cout << "Enter appointment start time (24 hour):";
	   cin >> begHour;
	   cin.ignore();
	   cout << "Enter start time minutes:";
	   cin >> begMin;
	   cin.ignore();
	   cout << "Enter appointment end time (24 hour):";
	   cin >> endHour;
	   cin.ignore();
	   cout << "Enter end time minutes:";
	   cin >> endMin;
	   cin.ignore();
	   cout << "Enter appointment title:";
	   getline(cin, apptText);
	   //add the appointment to the specific day
	   cal.addToDay(viewDay, errandType, begHour, begMin, endHour, endMin,
			apptText);
	   //display success message
	   cout << "Appointment successfully added." << endl;
	 }
	   break;

	 case 3: {
	   //if user enters exit
	   cout << "Returning to main menu...\n" << endl;
	 }
	   break;

	 default: {
	   //if user enters invalid option
	   cout << "Invalid option selected." << endl;
	 }
	   break;
	 }
       }
	 break;

       case 4:  {
	 int viewDay;
	 int deleteErr;
	 cal.toString();
	 cout << "Please select a day to remove an errand from:";
	 //store day user selected
	 cin >> viewDay;
	 //clear cin
	 cin.ignore();
	 //displays in-depth view of current day
	 cal.displayAgenda(viewDay);
	 cout << "Which would you like to delete?" << endl <<
	   "Please enter as a number, e.g. - '1'";
	 //store errand user would like to delete
	 cin >> deleteErr;
	 //clear cin
	 cin.ignore();
	 //remove errand from the day
	 cal.removeFromDay(viewDay, deleteErr);
       }
	 break;

       case 5: {
	 string fileName;
	 //ask user for filename
	 cout << "Please enter a name for the file:";
	 getline(cin, fileName);
	 //write calendar to file
	 cal.writeToFile(fileName);
	 //display success message
	 cout << "Successfully saved calendar to file." << endl;
       }
	 break;

       case 6: {
	 //exit 
	 cout << "Thank you for using this calendar tool!" << endl;
       }
	 break;

       default: {
	 //user selected invalid option
	 cout << "Invalid option selected" << endl;
       }
	 break;
       }
       //exit do-while if user selects 6
     } while(userSel != 6);
   }
    else if(userCalChoice == 'P') {
      //declare variables to store user input
      string calName;
      int number;
      string beginCalDay;
      //store user input
      cout << "Please enter your calendar title: ";
      getline(cin, calName);
      cout << "Please enter the number of days for your calendar: ";
      cin >> number;
      //clear cin
      cin.ignore();
      //display menu of options to the user
      cout << "Please enter the day your calendar starts on: " << endl 
	   << "1. Monday" << endl << "2. Tuesday" << endl << "3. Wednesday"
	   << endl << "4. Thursday" << endl << "5. Friday" << endl
	   << "6. Saturday" << endl << "7. Sunday" << endl;
      getline(cin, beginCalDay);
      cout << "\nMaking your calendar..." << endl;
      Calendar userCal(calName, number, beginCalDay);
      cout << "Successfully built your calendar.\n" << endl;
      int userSel;
      do{
	cout << "Menu:" << endl << "1. Display calendar" << endl <<
	   "2. Display a day in-depth"
	     << endl << "3. Add an errand" << endl << "4. Delete an errand" << endl << "5. Save calendar to a file" << endl << "6. Exit" << endl;
	cin >> userSel;
	cin.ignore();
	
	/*
	  The switch case below follows the same logic as the 
	  "default" calendar option. Therefore, method comments 
	  are ommitted. 
	 */
	switch(userSel) {
	case 1: {
	  userCal.toString();
	}
	  break;

	case 2: {
	  int viewDay;
	  userCal.toString();
	  cout << "Which day would you like to view?\n"
	    "Please enter as number - e.g. '1'" << endl;
	  cin >> viewDay;
	  cin.ignore();
	  userCal.displayAgenda(viewDay);
	}
	  break;

	case 3: {
	  int viewDay;
	  int errandType;
	  int userSel;
	  userCal.toString();
	  cout << "Which day would you like to add an errand to?" << endl
	       << "Please enter as number - e.g. '1'" << endl;
	  cin >> viewDay;
	  cin.ignore();
	  cout << "What kind of errand would you like to add?" << endl
	       << "1. Reminder" << endl << "2. Appointment" << endl <<
	    "3. Return to main menu" << endl;
	  cin >> userSel;
	  cin.ignore();

	  switch(userSel) {
	  case 1: {
	    string remText;
	    errandType = 1;
	    cout << "Enter reminder text:";
	    getline(cin,remText);
	    userCal.addToDay(viewDay,errandType, 0,0,0,0,remText);
	    cout << "Reminder was successfully added." << endl;
	  }
	    break;

	  case 2: {
	    int begHour;
	    int begMin;
	    int endHour;
	    int endMin;
	    string apptText;
	    errandType = 2;
	    cout << "Enter appointment start time (24 hour):";
	    cin >> begHour;
	    cin.ignore();
	    cout << "Enter start time minutes:";
	    cin >> begMin;
	    cin.ignore();
	    cout << "Enter appointment end time (24 hour):";
	    cin >> endHour;
	    cin.ignore();
	    cout << "Enter end time minutes:";
	    cin >> endMin;
	    cin.ignore();
	    cout << "Enter appointment title:";
	    getline(cin, apptText);
	    userCal.addToDay(viewDay, errandType, begHour, begMin, endHour, endMin,
			 apptText);
	    cout << "Appointment successfully added." << endl;
	  }
	    break;

	  case 3: {
	    cout << "Returning to main menu...\n" << endl;
	  }
	    break;

	  default: {
	    cout << "Invalid option selected." << endl;
	  }
	    break;
	  }
	}
	  break;

	case 4:  {
	  int viewDay;
	  int deleteErr;
	  userCal.toString();
	  cout << "Please select a day to remove an errand from:";
	  cin >> viewDay;
	  cin.ignore();
	  userCal.displayAgenda(viewDay);
	  cout << "Which would you like to delete?" << endl <<
	    "Please enter as a number, e.g. - '1'";
	  cin >> deleteErr;
	  cin.ignore();
	  userCal.removeFromDay(viewDay, deleteErr);
	}
	  break;

	case 5: {
	  string fileName;
	  cout << "Please enter a name for the file:";
	  getline(cin, fileName);
	  userCal.writeToFile(fileName);
	  cout << "Successfully saved calendar to file." << endl;
	}
	  break;

	case 6: {
	  cout << "Thank you for using this calendar tool!" << endl;
	}
	  break;

	default: {
	  cout << "Invalid option selected" << endl;
	}
	  break;
	}
      } while(userSel != 6);
    }
  }
    break;

  case 2: {
    cout << "Exited.\n" << "Thank you for using this calendar tool!" << endl;
  }
    break;

  default: { 
    cout << "Invalid option entered." << endl;
  }  
    break;
 }
  return 0;
}

