/*
Author: Sruti Ganti
Date: 4/19/21 
Description: This class represents a reminder that can be 
either added or deleted from the calendar.
*/

//libraries to include
#include <iostream>
#include <string>
#include <vector>
#include "Reminder.h"
using namespace std;

//Class constructor that takes in reminder text
Reminder::Reminder(string text) {
    reminder = text;
  }

//method that prints the contents of the reminder
void Reminder::toString() {
    cout << "Reminder: " << reminder << "\n";
  }

