/*
Author: Sruti Ganti
Date: 4/20/21
Description: This is the header file that contains all 
class data for the Appointment.cpp class.
*/

#ifndef APPOINTMENT_H
#define APPOINTMENT_H

#include <iostream>
#include <string>
#include <vector>
#include "Reminder.h"
using namespace std;

class Appointment: public Reminder {
private:
  int beginHour;
  int beginMin;
  int endHour;
  int endMin;

public:
  Appointment(int begHr, int begM, int endHr, int endM, string text);
  void toString();
};

#endif // APPOINTMENT_H
